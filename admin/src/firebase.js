// Import the functions you need from the SDKs you need
import { initializeApp } from 'firebase/app'
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: 'AIzaSyDQYiFLbEU0aY8ID6eA6nezuhkMX4ZTJFs',
  authDomain: 'shop-23fc6.firebaseapp.com',
  projectId: 'shop-23fc6',
  storageBucket: 'shop-23fc6.appspot.com',
  messagingSenderId: '29401010011',
  appId: '1:29401010011:web:e1fb661969c3874766947e',
}

// Initialize Firebase
const app = initializeApp(firebaseConfig)
export default app
