import Sidebar from './components/sidebar/Sidebar'
import Topbar from './components/topbar/Topbar'
import './App.css'
import Home from './pages/home/Home'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import UserList from './pages/userList/UserList'
import User from './pages/user/User'
import NewUser from './pages/newUser/NewUser'
import ProductList from './pages/productList/ProductList'
import Product from './pages/product/Product'
import NewProduct from './pages/newProduct/NewProduct'
import Login from './pages/home/sign/Login'
import { Fragment } from 'react'

function App() {
  const admin = JSON.parse(
    JSON.parse(localStorage.getItem('persist:root')).user
  ).currentUser.isAdmin
  return (
    <BrowserRouter>
      <Routes>
                <Route
          path='/login'
          element={admin ? <Navigate to='/' /> : <Login />}
        ></Route>
      </Routes>
      {admin && (
        <Fragment>
          <Topbar />
          <div className='container'>
            <Sidebar />
            <Routes>
              <Route path='/' element={<Home />}></Route>
              <Route path='/users' element={<UserList />}></Route>
              <Route path='/user/:userId' element={<User />}></Route>
              <Route path='/newUser' element={<NewUser />}></Route>
              <Route path='/products' element={<ProductList />}></Route>
              <Route path='/product/:productId' element={<Product />}></Route>
              <Route path='/newproduct' element={<NewProduct />}></Route>
            </Routes>
          </div>
        </Fragment>
      )}
    </BrowserRouter>
  )
}

export default App
