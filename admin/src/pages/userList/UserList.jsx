import './userList.css'
import { DataGrid } from '@mui/x-data-grid'
import { DeleteOutline } from '@mui/icons-material'
import { userRows } from '../../dummyData'
import { Link } from 'react-router-dom'
import { Fragment, useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getUsers } from '../../redux/apiCalls'

export default function UserList() {
  const user = useSelector((state) => state.user.currentUser)
  const dispatch = useDispatch()
  useEffect(() => {
    getUsers(dispatch)
  }, [dispatch])

  const columns = [
    { field: '_id', headerName: 'ID', width: 90 },
    {
      field: 'username',
      headerName: 'User',
      width: 200,
      renderCell: (params) => {
        return (
          <div className='userListUser'>
            <img className='userListImg' src={params.row.img} alt='' />
            {params.row.username}
          </div>
        )
      },
    },
    { field: 'email', headerName: 'Email', width: 200 },

    {
      field: 'action',
      headerName: 'Action',
      width: 150,
      renderCell: (params) => {
        return (
          <Fragment>
            <Link to={'/user/' + params.row._id}>
              <button className='userListEdit'>Edit</button>
            </Link>
            <DeleteOutline className='userListDelete' />
          </Fragment>
        )
      },
    },
  ]

  return (
    <div className='userList'>
      <DataGrid
        rows={user}
        getRowId={(row) => row._id}
        disableSelectionOnClick
        columns={columns}
        pageSize={8}
        checkboxSelection
      />
    </div>
  )
}
