import { puplicRequest, userRequest } from '../requestMethods'
import {
  addProductFailure,
  addProductStart,
  addProductSuccess,
  deleteProductFailure,
  deleteProductStart,
  deleteProductSuccess,
  getProductFailure,
  getProductStart,
  getProductSuccess,
  updateProductFailure,
  updateProductStart,
  updateProductSuccess,
} from './productRedux'
import {
  RegUserFailure,
  RegUserStart,
  RegUserSucssess,
  getUserFailure,
  getUserStart,
  getUserSucssess,
  loginFailure,
  loginStart,
  loginSucssess,
  logoutSucssess,
} from './userSlice'

export const login = async (dispatch, user) => {
  dispatch(loginStart())
  try {
    const res = await puplicRequest.post('/auth/login', user)
    dispatch(loginSucssess(res.data))
  } catch (err) {
    dispatch(loginFailure())
  }
}
export const logout = (dispatch) => {
  dispatch(logoutSucssess())
}
export const getUsers = async (dispatch) => {
  dispatch(getUserStart())
  try {
    const res = await userRequest.get('/users')
    dispatch(getUserSucssess(res.data))
  } catch (err) {
    dispatch(getUserFailure())
  }
}
export const addUsers = async (user, dispatch) => {
  dispatch(RegUserStart())
  try {
    const res = await userRequest.post(`/register`, user)
    dispatch(RegUserSucssess(res.data))
  } catch (err) {
    dispatch(RegUserFailure())
  }
}

export const getProducts = async (dispatch) => {
  dispatch(getProductStart())
  try {
    const res = await puplicRequest.get('/products')
    dispatch(getProductSuccess(res.data))
  } catch (err) {
    dispatch(getProductFailure())
  }
}

export const deletProducts = async (id, dispatch) => {
  dispatch(deleteProductStart())
  try {
    // const res = await userRequest.delete(`/products/${id}`)
    dispatch(deleteProductSuccess(id))
  } catch (err) {
    dispatch(deleteProductFailure())
  }
}
export const updateProducts = async (id, product, dispatch) => {
  dispatch(updateProductStart())
  try {
    const res = await userRequest.put(`/products/${id}`)
    dispatch(updateProductSuccess({ id, product }))
  } catch (err) {
    dispatch(updateProductFailure())
  }
}
export const addProducts = async (product, dispatch) => {
  dispatch(addProductStart())
  try {
    const res = await userRequest.post(`/products`, product)
    dispatch(addProductSuccess(res.data))
  } catch (err) {
    dispatch(addProductFailure())
  }
}
