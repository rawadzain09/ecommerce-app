import { createSlice } from '@reduxjs/toolkit'

const userSlice = createSlice({
  name: 'user',
  initialState: {
    currentUser: null,
    isFatching: false,
    error: false,
  },
  reducers: {
    loginStart: (state) => {
      state.isFatching = true
    },
    loginSucssess: (state, action) => {
      state.isFatching = false
      state.currentUser = action.payload
    },
    loginFailure: (state) => {
      state.isFatching = false
      state.error = true
    },

    getUserStart: (state) => {
      state.isFatching = true
    },
    getUserSucssess: (state, action) => {
      state.isFatching = false
      state.currentUser = action.payload
    },
    getUserFailure: (state) => {
      state.isFatching = false
      state.error = true
    },

    RegUserStart: (state) => {
      state.isFatching = true
    },
    RegUserSucssess: (state, action) => {
      state.isFatching = false
      state.currentUser.push(action.payload)
    },
    RegUserFailure: (state) => {
      state.isFatching = false
      state.error = true
    },

    logoutSucssess: (state) => {
      state.isFatching = false
      state.currentUser = null
    },
  },
})
export const {
  loginStart,
  loginSucssess,
  loginFailure,
  getUserStart,
  getUserSucssess,
  getUserFailure,
  RegUserFailure,
  RegUserStart,
  RegUserSucssess,

  logoutSucssess,
} = userSlice.actions
export default userSlice.reducer
