import React from 'react'
import ProductList from './pages/categories/ProductList'
import Product from './pages/categories/Product'
import Home from './pages/home/Home'
import Register from './pages/signup/Register'
import Login from './pages/signup/Login'
import Cart from './pages/home/cart/Cart'
import { BrowserRouter, Routes, Route } from 'react-router-dom'
import { Navigate } from 'react-router-dom/dist'
import Sucess from './pages/home/Sucess'
import { useSelector } from 'react-redux'

function App() {
  const user = useSelector((state) => state.user.currentUser)
  return (
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/products/:category' element={<ProductList />}></Route>
        <Route path='/success' element={<Sucess />}></Route>
        <Route path='/product/:id' element={<Product />}></Route>
        <Route path='/cart' element={<Cart />}></Route>
        <Route path='/login' element={user ? <Navigate to='/' /> : <Login />} />
        <Route
          path='/register'
          element={user ? <Navigate to='/' /> : <Register />}
        ></Route>
      </Routes>
    </BrowserRouter>
  )
}

export default App
