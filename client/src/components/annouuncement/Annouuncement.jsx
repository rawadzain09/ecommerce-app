import { styled } from 'styled-components'

const Container = styled.div`
  width: auto;
  height: 30px;
  background-color: teal;
  color: white;
  display: flex;
  align-items: center;
  justify-content: center;
  font-size: 14px;
  font-weight: 500;
`

function Annouuncement() {
  return <Container>Super Deal! Free Shopping on OrdereOver $50 </Container>
}

export default Annouuncement
