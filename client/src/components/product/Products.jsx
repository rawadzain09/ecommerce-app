import { styled } from 'styled-components'
import Product from './Product'
import { useEffect, useState } from 'react'
import axios from 'axios'

const Container = styled.div`
  display: flex;
  padding: 20px;
  flex-wrap: wrap;
  justify-content: space-between;
`

function Products({ cat, filter, sort }) {
  const [products, setProducts] = useState([])
  const [filterProduct, setFilterProducts] = useState([])

  useEffect(() => {
    const getProducts = async () => {
      try {
        const res = await axios.get(
          cat
            ? `http://localhost:5000/api/products?category=${cat}`
            : 'http://localhost:5000/api/products'
        )
        setProducts(res.data)
      } catch (error) {}
    }
    getProducts()
  }, [cat])

  useEffect(() => {
    cat &&
      setFilterProducts(
        products.filter((item) =>
          Object.entries(filter).every(([key, value]) =>
            item[key].includes(value)
          )
        )
      )
  }, [products, cat, filter])

  useEffect(() => {
    if (sort === 'newest') {
      setFilterProducts((prev) =>
        [...prev].sort((a, b) => a.createdAt - b.createdAt)
      )
    } else if (sort === 'asc') {
      setFilterProducts((prev) => [...prev].sort((a, b) => a.price - b.price))
    } else {
      setFilterProducts((prev) => [...prev].sort((a, b) => b.price - a.price))
    }
  }, [sort])

  return (
    <Container>
      {cat
        ? filterProduct.map((item) => (
            <Product item={item} key={item._id}></Product>
          ))
        : products
            .slice(0, 7)
            .map((item) => <Product item={item} key={item._id}></Product>)}
    </Container>
  )
}

export default Products
