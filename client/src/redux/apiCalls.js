import { puplicRequest } from '../requestMethods'
import {
  loginFailure,
  loginStart,
  loginSucssess,
  logoutSucssess,
} from './userSlice'

export const login = async (dispatch, user) => {
  dispatch(loginStart())
  try {
    const res = await puplicRequest.post(
      '/auth/login',

      user
    )
    dispatch(loginSucssess(res.data))
  } catch (err) {
    dispatch(loginFailure())
  }
}
export const logout = (dispatch) => {
  dispatch(logoutSucssess())
}
