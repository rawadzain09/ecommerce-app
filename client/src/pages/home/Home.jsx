import React from 'react'
import NavBar from '../../components/navBar/NavBar'
import Annouuncement from '../../components/annouuncement/Annouuncement'
import Slider from '../../components/slider/Slider'
import Categories from '../../components/categories/Categories'
import Products from '../../components/product/Products'
import NewsLetter from '../../components/newsLrtter/NewsLetter'
import Footer from '../../components/footer/Footer'

function Home() {
  return (
    <div>
      <Annouuncement />
      <NavBar />
      <Slider />
      <Categories />
      <Products />
      <NewsLetter />
      <Footer />
    </div>
  )
}

export default Home
